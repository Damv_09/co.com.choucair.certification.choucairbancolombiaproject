#Autor: Rose
@stories
Feature: Academy Choucair
  As a user, I want to learn how to automate in screenplay at the Choucair Academy withthe automation course
  @scenario1
  Scenario Outline: Search for a automation course
    Given than Rose want to learn automation at the academy Choucair
      | strUser   | strPassword  |
      | <strUser> | <strPassword>|
    When she search for the course on the Choucair Academy platform
      | strCourse    |
      | <strCourse>  |
    Then she finds the course called
      |strCourse     |
      | <strCourse>  |
    Examples:
      | strUser    | strPassword      | strCourse               |
      | 1061799606 |Choucair2021*     |Metodología Bancolombia  |