package co.com.choucair.certification.choucairbancolombiaproject.userinterface;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SearchCoursePage extends PageObject {
    public static final Target BUTTON_UC = Target.the("section of choucari university ").
            located(By.xpath("//div[@id='universidad']//strong"));
    public static final Target INPUT_COURSE = Target.the("Where do we write the course").
            located(By.id("coursesearchbox"));
    public static final Target BUTTON_GO = Target.the("button for search").
            located(By.xpath("//button[@class='btn btn-secondary']"));
    public static final Target SELECT_COURSE = Target.the("Select the Course we are looking for ").
            located(By.xpath("//h4[contains(text(),'Recursos Automatización Bancolombia')]"));
    public static final Target NAME_COURSE = Target.the("bring the name the course").
            located(By.xpath("//h1[contains(text(),'Recursos Automatización Bancolombia')]"));

}
